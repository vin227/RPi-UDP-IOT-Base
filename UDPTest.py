import socket
from gpiozero import LED
from time import sleep
cd = LED(17)
tuner = LED(27)
power = LED(22)

UDP_IP = "192.168.10.42"
UDP_PORT = 8888
REPLY_PORT = 8899
REPLY_MESSAGE = "amlivingroom"
EXPECTED_QUERY = "is arduino yes"
sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.bind(('', UDP_PORT))

cd.off()
power.off()
tuner.off()

while True:
	data, addr = sock.recvfrom(1024)
	if data == "is arduino yes":
		print "got query"
		lst = list(addr)
		print "replying to", lst[0],":",REPLY_PORT
		sock.sendto(REPLY_MESSAGE, (lst[0], REPLY_PORT))
	elif data == "cd":
		print "cd"
		cd.on()
		sleep(1)
		cd.off()
	elif data == "tuner":
		print "tuner"
		tuner.on()
		sleep(1)
		tuner.off()
	elif data == "poweron":
		power.on()
		print "poweron"
	elif data == "poweroff":
		power.off()
		print "poweroff"
	else:
		print "received:", data